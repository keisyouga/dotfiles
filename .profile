# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		. "$HOME/.bashrc"
	fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
	    PATH="$HOME/.local/bin:$PATH"
fi

# set PAGER to less or more
if which less > /dev/null ; then
	PAGER="$( which less )"
else
	PAGER=more
fi
export PAGER

# change emacs socket name from XDG_RUNTIME_DIR/emacs/server
# the XDG_RUNTIME_DIR directory is removed when login and logout
EMACS_SOCKET_NAME=$HOME/.emacs.d/socket/server
export EMACS_SOCKET_NAME

# set EDITOR to emacsclient or vi
if which emacsclient > /dev/null ; then
	# `emacsclient -c' is not supported in emacs term buffer
	# see ~/.bashrc
	EDITOR="$( which emacsclient ) -c -a vi"
else
	EDITOR=vi
fi
export EDITOR

# set editor for lilypond-invoke-editor
LYEDITOR=emacs
export LYEDITOR

export LYNX_CFG=$HOME/lynx.cfg

# run fortune
if command -v fortune > /dev/null ; then
	# use cowsay if available
	if command -v cowsay > /dev/null && command -v shuf > /dev/null ; then
		fortune | cowsay -f `ls /usr/share/cowsay/cows | shuf -n 1`
	else
		fortune
	fi
fi
