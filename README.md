# my dotfiles

## setup
```sh
cd $HOME
git clone URL
ln -s dotfiles/.bashrc ./
ln -s dotfiles/.profile ./
  .
  .
  .
cp -i dotfiles/.gitconfig ~/.gitconfig
git config --global user.name USERNAME
git config --global user.email MAIL@ADDRESS
mkdir ~/.irssi
cp -i dotfiles/.irssi/config ~/.irssi/config
# set real_name, user_name, nick
editor ~/.irssi/config
```
