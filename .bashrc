# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
# do not truncate history file (bash version 4.3 or later)
HISTFILESIZE=-1

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# warn first attempt to exit bash, if jobs are running
shopt -s checkjobs

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# `\w' in PS1, show like `.../current/dir'
PROMPT_DIRTRIM=2
#PS1='\[\033[01;32m\]\u\[\033[00;32m\]@\h\[\033[00m\]:\[\033[34;47m\]\w\[\033[00m\]\$ '
GIT_PS1_SHOWDIRTYSTATE=1

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

if type __git_ps1 >/dev/null 2>/dev/null ; then
	PS1='\[\033[01;32m\]\u\[\033[00;32m\]@\h\[\033[00m\]:\[\033[36;47m\]\w\[\033[00m\]$(__git_ps1 " (%s)")\n\$ '
else
	PS1='\[\033[01;32m\]\u\[\033[00;32m\]@\h\[\033[00m\]:\[\033[36;47m\]\w\[\033[00m\]\n\$ '
fi

export LESS='-R -i -X -S -F'
export LESS_TERMCAP_mb=$(printf "\e[1;31m")
export LESS_TERMCAP_md=$(printf "\e[1;31m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf "\e[1;44;33m")
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[1;32m")

# disable start/stop output control
# this command enables forward-search-history (C-s) and quoted-insert (C-q)
stty -ixon

# emacs terminal setting
case "$TERM" in
	eterm*)
		# `emacsclient -c' is not supported in emacs term buffer
		# see ~/.profile
		EDITOR="$( which emacsclient ) -a vi"
esac
