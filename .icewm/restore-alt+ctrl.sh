# restore disabled alt+ctrl+? hotkeys script
# assumes ~/.icewm/preferences.orig and ~/.icewm/keys.orig are exists

mv ~/.icewm/preferences.orig ~/.icewm/preferences
mv ~/.icewm/keys.orig ~/.icewm/keys
icewm -r
