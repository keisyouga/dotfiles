# disable alt+ctrl+? hotkeys script

# modify ~/.icewm/preferences
#   # KeySysAddressBar="Alt+Ctrl+Space"
#   =>
#   # KeySysAddressBar="Alt+Ctrl+Space"
#   KeySysAddressBar=""
sed -i.orig ~/.icewm/preferences -e '/^#.*Alt+Ctrl/{p;s/".*"/""/;s/^# *//}'

# modify ~/.icewm/keys
#   key "Ctrl+Alt+Up" icesh -w focus above
#   key "Alt+Ctrl+Down" icesh -w focus below
#   =>
#   #key "Ctrl+Alt+Up" icesh -w focus above
#   #key "Alt+Ctrl+Down" icesh -w focus below
sed -i.orig ~/.icewm/keys -e '/^key "Alt+Ctrl/s/^.*$/#&/;/^key "Ctrl+Alt/s/^.*$/#&/'

# restart icewm
icewm -r
