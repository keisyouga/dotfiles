
!!! for some application font size
Xft.dpi: 96

!!! xterm
XTerm*termName: xterm-256color
UXTerm*termName: xterm-256color
*VT100.metaSendsEscape: true

!! freetype font
! command line: -fs
!*VT100.faceSize: 9
! command line: -fa
*VT100.faceName: DejaVu Sans Mono:pixelsize=12
! command line: -fd
!*VT100.faceNameDoublesize: IPAGothic:pixelsize=14
*VT100.faceNameDoublesize: WenQuenYi Zen Hei:pixelsize=14

! set font for preedit string in x input method
! set *, make the xterm a little slower to start
! specify command line `xterm -fx <fontset>' or `xterm -xrm "*ximFont: <fontset>"'
!*VT100.ximFont: *

!! bitmap font
!*VT100.utf8: false
! command line: -fn
!*VT100.font: 8x16
!*VT100.font: -misc-ipagothic-medium-r-normal--16-0-0-0-c-0-iso8859-1

!! 8x13
!*VT100.utf8Fonts.font: 8x13
! command line: -fw
!*VT100.wideFont: -misc-fixed-medium-r-normal-ja-13-120-75-75-c-120-iso10646-1

!! ipa font
!*VT100.utf8Fonts.font: 8x16
!*VT100.utf8Fonts.font: -misc-ipagothic-medium-r-normal--16-0-0-0-c-0-iso8859-1
!*VT100.wideFont: -misc-ipagothic-medium-r-normal--16-0-0-0-c-0-iso10646-1

!! lucidasanstypewriter
!*VT100.utf8Fonts.font: lucidasanstypewriter-12
!*VT100.wideFont: -misc-fixed-medium-r-normal-ja-18-120-100-100-c-180-iso10646-1

*VT100.color4: lightblue
*VT100.reverseVideo: true
!*VT100.scrollBar: true
*VT100.modifyOtherKeys: 1

!!! xclock
XClock.Clock.analog: true
!! enable second hand on analog clock
XClock.Clock.update: 0.25

!!! rxvt-unicode
URxvt.transparent: true
URxvt.tintColor: cyan
URxvt.shading: 80
URxvt.reverseVideo: true
URxvt.scrollBar: false
!! corefont
URxvt.font: 8x16,-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-5,kanji16,hanzigb16fs,-daewoo-gothic-medium-r-normal--16-120-100-100-c-160-ksc5601.1987-0
URxvt.boldFont: 8x16,-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-5,kanji16,hanzigb16fs,-daewoo-gothic-medium-r-normal--16-120-100-100-c-160-ksc5601.1987-0
URxvt.italicFont: 8x16,-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-5,kanji16,hanzigb16fs,-daewoo-gothic-medium-r-normal--16-120-100-100-c-160-ksc5601.1987-0
URxvt.boldItalicFont: 8x16,-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-5,kanji16,hanzigb16fs,-daewoo-gothic-medium-r-normal--16-120-100-100-c-160-ksc5601.1987-0
!! xftfont
! URxvt.font: xft:noto mono:pixelsize=16
! URxvt.letterSpace: -2

Xfd*quit.Translations:  #override \
<Btn1Down>,<Btn1Up>: Quit() unset()
Xfd*next16.Translations:  #override \
<Btn1Down>,<Btn1Up>: Next16() unset()
Xfd*next.Translations:  #override \
<Btn1Down>,<Btn1Up>: Next() unset()
Xfd*prev.Translations:  #override \
<Btn1Down>,<Btn1Up>: Prev() unset()
Xfd*prev16.Translations:  #override \
<Btn1Down>,<Btn1Up>: Prev16() unset()

Xfd*Translations: #override \n\
<Key>q: Quit()\n\
<Key>Down: Next()\n\
<Key>n: Next()\n\
<Key>Up: Prev()\n\
<Key>p: Prev()\n\
<Key>Right: Next16()\n\
<Key>Next: Next16()\n\
<Key>f: Next16()\n\
<Key>Left: Prev16()\n\
<Key>Prior: Prev16()\n\
<Key>b: Prev16()

!!! emacs
!! note : some fontsets cannot display italic
!emacs.font: fontset-standard
! emacs.font: 8x16
! emacs.font: lucidasanstypewriter-10
! emacs.font: WenQuanYi Micro Hei Mono-10
!! _ (underscore) of ipagothic or ipamincho are not visible at specific font size.
!! in my environment, not visible size is : 1 2 3 5 6 7 8 9 12 13 14 15 20 21 22 28.
! emacs.font: IPAGothic-11
! emacs.font: IPAMincho-11

!! menubar off
emacs.menuBar: off
!! toolbar off
emacs.toolBar: off

! emacs.background: black
! emacs.foreground: white
! emacs.reverseVideo: true
